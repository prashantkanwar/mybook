﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace my_books.Migrations
{
    public partial class PublisherandBook : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PublishersId",
                table: "Books",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Books_PublishersId",
                table: "Books",
                column: "PublishersId");

            migrationBuilder.AddForeignKey(
                name: "FK_Books_Publishers_PublishersId",
                table: "Books",
                column: "PublishersId",
                principalTable: "Publishers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Books_Publishers_PublishersId",
                table: "Books");

            migrationBuilder.DropIndex(
                name: "IX_Books_PublishersId",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "PublishersId",
                table: "Books");
        }
    }
}
