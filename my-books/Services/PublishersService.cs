﻿using my_books.Data;
using my_books.Data.Paging;
using my_books.Exceptions;
using my_books.Models;
using my_books.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace my_books.Services
{
    public class PublishersService
    {
        private readonly AppDbContext _context;
        public PublishersService(AppDbContext context)
        {
            _context = context;
        }

        public List<Publisher> GetAllPublishers(string sortBy, string searchString, int? pageNumber) 
        {
            var allPublisher = _context.Publishers.OrderBy(n => n.Name).ToList();
            if(!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy)
                {
                    case "name_desc":
                        allPublisher = allPublisher.OrderByDescending(n => n.Name).ToList();
                        break;
                    default:
                        break;
                }
            }
            if(!string.IsNullOrEmpty(searchString))
            {
                allPublisher = allPublisher.Where(n => n.Name.Contains(searchString, StringComparison.CurrentCultureIgnoreCase)).ToList();
            }

            //Paging
            int pageSize = 5;
            allPublisher = PaginatedList<Publisher>.Create(allPublisher.AsQueryable(), pageNumber ?? 1, pageSize);
            return allPublisher;
        } 

        public void AddPublisher(PublisherVM book)
        {
            if (StringStartWithNumber(book.Name)) throw new PublisherNameException("Name strats with number", book.Name);
            var _publisher = new Publisher()
            {
                Name = book.Name
            };
            _context.Publishers.Add(_publisher);
            _context.SaveChanges();
        }

        public PublisherWithBooksAndAuthorVM GetPUblisherData(int publisherId)
        {
            var _publisherData = _context.Publishers.Where(n => n.Id == publisherId).Select(n => new PublisherWithBooksAndAuthorVM()
            {
                Name = n.Name,
                BookAuthors = n.Books.Select(n => new BookAuthorVM()
                {
                    BookName = n.Title,
                    BookAuthors = n.Book_Authors.Select(n => n.Author.FullName).ToList()
                }).ToList()
            }).FirstOrDefault();
            return _publisherData;
        }

        public void DeletePublisherById(int id)
        {
            var _publisher = _context.Publishers.FirstOrDefault(n => n.Id == id);
            if(_publisher != null)
            {
                _context.Publishers.Remove(_publisher);
                _context.SaveChanges();
            }
        }

        private bool StringStartWithNumber(string name)
        {
            if (Regex.IsMatch(name, @"^\d")) return true;
            return false;
        }
    }
}
